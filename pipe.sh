#!/usr/bin/env bash
#
# This pipe is the implementation of a bitbucket pipe
#

echo "Executing the pipe..."

# Default parameters
DEBUG=${DEBUG:="false"}

# write a file whit the credentials for kubernetes
echo "${GOOGLE_CLIENT_SECRET}" > "${FILE_SECRECT}"

# save the contetnt of the secrect file in a env var
export GOOGLE_APPLICATION_CREDENTIALS="${FILE_SECRECT_PATH}"/"${FILE_SECRECT}"

# define the clister name in a env var
export CLUSTER=$CLUSTER$MODULE

# define the image paht in the registry in a env var
export IMAGE_PATH=$IMAGE_PATH/$MODULE

# define the full name of the image whit the registry path
export IMAGE_NAME=$REGISTRY/$PROJECT_ID/$IMAGE_PATH/$IMAGE:$TAG

# set the project config for the google-cloud-sdk
gcloud config set project "${PROJECT_ID}"

# confifg the account in the google-cloud-sdk
gcloud config set account "${MAVEN_USER}"

# authenticate the cli whit the secrect file
gcloud auth activate-service-account --key-file "${FILE_SECRECT}"

# install thr maven dependences
mvn clean install -DskipTests

# build the docker image whit tag
docker build -t "${IMAGE_NAME}" .

# login the docker daemon this the google cloud regisgtry
gcloud auth configure-docker -q

# push the image to the registry
docker push "${IMAGE_NAME}"

# get the credentiasl for the cluster
gcloud container clusters get-credentials "${CLUSTER}" --project="${PROJECT_ID}" --region="${REGION}"

# implement the new version of the image to the kubern etes cluster
kubectl set image deployments/"${SERVICE_NAME}" "${SERVICE_NAME}"="${IMAGE_NAME}"
