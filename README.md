# CI/CD pipe executor 1.0

##### mirai

## Requirements

* Add the follow code to the bitbucket-pipelines.yml file

```
- pipe: dockerhub-image
  environment:
    REGISTRY: $REGISTRY
    MODULE: $MODULE
    REGION: $REGION
    IMAGE_PATH: $IMAGE_PATH
    MAVEN_USER: $MAVEN_USER
    SERVICE_PATH: $SERVICE_PATH
    IMAGE: $IMAGE
    TAG: $TAG
    CLUSTER: $CLUSTER
    DEPLOYMENT_FILE: $DEPLOYMENT_FILE
    FILE_SECRECT: $FILE_SECRECT
    SERVICE_NAME: $SERVICE_NAME
    PROJECT_ID: $PROJECT_ID
    GOOGLE_CLIENT_SECRET: $GOOGLE_CLIENT_SECRET
    FILE_SECRECT_PATH: $FILE_SECRECT_PATH
```

## build and deploy

This pipe take the task of build and deploy the docker images
